<?php


namespace Drupal\senapi_social\Plugin\Block;


use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\senapi_social\FacebookTimelineWidget;
use Drupal\senapi_social\FacebookWidgetInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class FacebookBlockBase extends BlockBase implements ContainerFactoryPluginInterface {

  protected $facebookWidget;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, FacebookWidgetInterface $facebook_widget) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->facebookWidget = $facebook_widget;
  }

  public function build() {
    return $this->facebookWidget->getWidget($this->configuration);
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('senapi_social.facebook_timeline_widget')
    );
  }

  public function defaultConfiguration() {
    return FacebookTimelineWidget::getDefaultSettings() + parent::defaultConfiguration();
  }

  public function blockForm($form, FormStateInterface $form_state) {
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#default_value' => $this->configuration['username'],
      '#required' => TRUE,
      '#maxlength' => FacebookWidgetInterface::USERNAME_MAX_LENGTH,
      '#size' => FacebookWidgetInterface::USERNAME_MAX_LENGTH,
    ];

    $settingsForm = $this->facebookWidget->getSettingsForm($this->configuration);

    return $form + $settingsForm;
  }

  public function blockSubmit($form, FormStateInterface $form_state) {
    foreach ($this->facebookWidget->getAvailableSettings() as $key => $setting) {
      if (is_array($setting)) {
        foreach ($setting as $childKey => $childSetting) {
          $this->configuration[$childKey] = $form_state->getValue([$key, $childKey]);
        }
      }
      else {
        $this->configuration[$key] = $form_state->getValue($key);
      }
    }
  }
}
<?php

namespace Drupal\senapi_social\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\senapi_social\TwitterTimelineWidget;
use Drupal\senapi_social\TwitterWidgetInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TwitterBlockBase
 *
 * @package Drupal\senapi_social\Plugin\Block
 */
abstract class TwitterBlockBase extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\senapi_social\TwitterWidgetInterface
   */
  protected $twitterWidget;

  /**
   * TwitterBlockBase constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\senapi_social\TwitterWidgetInterface $twitter_widget
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TwitterWidgetInterface $twitter_widget) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->twitterWidget = $twitter_widget;
  }

  /**
   * @return array
   */
  public function build() {
    return $this->twitterWidget->getWidget($this->configuration);
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return \Drupal\Core\Plugin\ContainerFactoryPluginInterface|\Drupal\senapi_social\Plugin\Block\TwitterBlockBase
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('senapi_social.twitter_timeline_widget')
    );
  }

  /**
   * @return array
   */
  public function defaultConfiguration() {
    return TwitterTimelineWidget::getDefaultSettings() + parent::defaultConfiguration();
  }

  /**
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#default_value' => $this->configuration['username'],
      '#required' => TRUE,
      '#field_prefix' => '@',
      '#maxlength' => TwitterWidgetInterface::USERNAME_MAX_LENGTH,
      '#size' => TwitterWidgetInterface::USERNAME_MAX_LENGTH,
    ];

    $settingsForm = $this->twitterWidget->getSettingsForm($this->configuration);
    $selector = 'settings';
    $settingsFormWithStates = $this->twitterWidget->setSettingsFormStates($settingsForm, $selector);

    return $form + $settingsFormWithStates;
  }

  /**
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    foreach ($this->twitterWidget->getAvailableSettings() as $key => $setting) {
      if (is_array($setting)) {
        foreach ($setting as $childKey => $childSetting) {
          $this->configuration[$childKey] = $form_state->getValue([$key, $childKey]);
        }
      }
      else {
        $this->configuration[$key] = $form_state->getValue($key);
      }
    }

    $this->twitterWidget->setDependentConfiguration($this->configuration);
  }
}
<?php

namespace Drupal\senapi_social\Plugin\Block;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Class FacebookTimelineBlock
 *
 * @Block(
 *   id = "facebook_timeline_block",
 *   admin_label = @Translation("Facebook Timeline")
 * )
 */
class FacebookTimelineBlock extends FacebookBlockBase implements ContainerFactoryPluginInterface {

}
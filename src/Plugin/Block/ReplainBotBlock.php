<?php


namespace Drupal\senapi_social\Plugin\Block;


use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ReplainBotBlock
 *
 * @Block(
 *   id = "replain_bot_block",
 *   admin_label = @Translation("Replain Bot")
 * )
 */
class ReplainBotBlock extends BlockBase {

  public function build() {
    return $this->getWidget($this->configuration);
  }

  public function defaultConfiguration() {
    return $this->getDefaultSettings() + parent::defaultConfiguration();
  }

  public function getDefaultSettings() {
    return [
      'replain_id' => '',
    ];
  }

  public function blockForm($form, FormStateInterface $form_state) {
    $form['replain_id'] = [
      '#type' => 'textfield',
      '#title' => t('Replain ID'),
      '#default_value' => $this->configuration['replain_id'],
      '#required' => TRUE,
      '#maxlength' => 36,
      '#size' => 36,
    ];

    return $form;
  }

  public function blockSubmit($form, FormStateInterface $form_state) {
    foreach ($this->getDefaultSettings() as $key => $setting) {
      if (is_array($setting)) {
        foreach ($setting as $childKey => $childSetting) {
          $this->configuration[$childKey] = $form_state->getValue([$key, $childKey]);
        }
      }
      else {
        $this->configuration[$key] = $form_state->getValue($key);
      }
    }
  }

  public function getWidget(array $configuration) {
    $build['telegram_chat'] = [
      '#type' => 'container',
      '#attached' => [
        'library' => ['senapi_social/replain_bot'],
        'drupalSettings' => [
          'replainId' => $configuration['replain_id']
        ]
      ]
    ];

    return $build;
  }
}
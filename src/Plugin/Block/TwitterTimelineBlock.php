<?php

namespace Drupal\senapi_social\Plugin\Block;


use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Class TwitterTimelineBlock
 *
 * @Block(
 *   id = "twitter_timeline_block",
 *   admin_label = @Translation("Twitter Timeline")
 * )
 */
class TwitterTimelineBlock extends TwitterBlockBase implements ContainerFactoryPluginInterface {

}
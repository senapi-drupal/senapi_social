<?php

namespace Drupal\senapi_social\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Class TabsFacebookTwitterTimelineBlock
 *
 * @Block(
 *   id = "facebook_twitter_timeline_block",
 *   admin_label = @Translation("Facebook Twitter Timeline")
 * )
 */

class TabsFacebookTwitterTimelineBlock extends BlockBase {

  /**
   * @inheritDoc
   */
  public function build() {
    return [
      '#type' => 'inline_template',
      '#template' => '
             <style type="text/css">.tabs-social {padding: 0!important; margin-bottom: 0!important;}</style>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
              <a class="nav-item nav-link active" id="nav-facebook-tab" data-toggle="tab" href="#nav-facebook" role="tab" aria-controls="nav-facebook" aria-selected="true">Facebook</a>
              <a class="nav-item nav-link" id="nav-twitter-tab" data-toggle="tab" href="#nav-twitter" role="tab" aria-controls="nav-twitter" aria-selected="false">Twitter</a>
            </div>
             <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-facebook" role="tabpanel" aria-labelledby="nav-facebook-tab">{{htmlFacebook}}</div>
                <div class="tab-pane fade" id="nav-twitter" role="tabpanel" aria-labelledby="nav-twitter-tab">{{htmlTwitter}}</div>             
             </div>',
      '#context' => [
        'htmlFacebook' => $this->renderHtml('facebooktimeline'),
        'htmlTwitter' => $this->renderHtml('twittertimeline'),
      ]
    ];
  }

  /**
   * @param $block_id
   * @return mixed
   */
  public function renderHtml($block_id) {
    $block = \Drupal\block\Entity\Block::load($block_id);
    $block_content = \Drupal::entityTypeManager()
      ->getViewBuilder('block')
      ->view($block);
    return \Drupal::service('renderer')->render($block_content);
  }
}
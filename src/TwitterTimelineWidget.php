<?php


namespace Drupal\senapi_social;


class TwitterTimelineWidget extends TwitterWidget implements TwitterWidgetInterface {

  public function getAvailableSettings() {
    return [
      'username' => '',
      'type' => 'profile',
      'type_value' => '',
      'display_style' => 'timeline',
      'display_options' => [
        'theme' => 'light',
        'chrome' => NULL,
        'width' => 0,
        'height' => 600,
        'link_color' => '#2b7bb9',
        'border_color' => '#000000',
        'tweet_limit' => 0,
        'show_replies' => FALSE,
        'aria_polite' => 'polite',
        'language' => '',
      ],
    ];
  }

  public static function getDefaultSettings() {
    return [
      'username' => '',
      'type' => 'profile',
      'type_value' => '',
      'display_style' => 'timeline',
      'theme' => 'light',
      'chrome' => NULL,
      'width' => 0,
      'height' => 600,
      'link_color' => '#2b7bb9',
      'border_color' => '#000000',
      'tweet_limit' => 0,
      'show_replies' => FALSE,
      'aria_polite' => 'polite',
      'language' => '',

      'hide_username' => FALSE,
      'hide_followers_count' => FALSE,
      'size' => NULL,
    ];
  }

  public function getSettingsForm(array $configuration) {
    $form = [];

    $form['type'] = [
      '#type' => 'radios',
      '#title' => t('Type'),
      '#options' => $this->getAvailableTypes(),
      '#default_value' => $configuration['type'],
      '#required' => TRUE,
    ];

    $form['type_value'] = [
      '#type' => 'textfield',
      '#title' => t('Type value'),
      '#default_value' => $configuration['type_value'],
      '#description' => t(' Applies to collection (collection id) or list (list name).'),
      '#maxlength' => 128,
      '#size' => 64
    ];

    $form['display_style'] = [
      '#type' => 'radios',
      '#title' => t('Display style'),
      '#description' => t('Grid is available for collection only.'),
      '#options' => $this->getAvailableDisplayStyles(),
      '#default_value' => $configuration['display_style'],
      '#required' => TRUE,
    ];

    $form['display_options'] = [
      '#type' => 'details',
      '#title' => t('Display options'),
      '#open' => FALSE,
    ];

    $form['display_options']['theme'] = [
      '#type' => 'radios',
      '#title' => t('Theme'),
      '#description' => t('Text and background colors.'),
      '#options' => [
        'light' => t('Light'),
        'dark' => t('Dark'),
      ],
      '#default_value' => $configuration['theme'],
      '#required' => TRUE,
    ];

    $form['display_options']['link_color'] = [
      '#type' => 'color',
      '#title' => t('Link color'),
      '#default_value' => $configuration['link_color'],
    ];

    $form['display_options']['border_color'] = [
      '#type' => 'color',
      '#title' => t('Border color'),
      '#default_value' => $configuration['border_color'],
    ];

    $form['display_options']['chrome'] = [
      '#type' => 'checkboxes',
      '#title' => t('Chrome'),
      '#options' => [
        'noheader' => t('No header'),
        'hofooter' => t('No footer'),
        'noborders' => t('No borders'),
        'transparent' => t('Transparent'),
        'noscrollbar' => t('No scrollbar'),
      ],
      '#default_value' => $configuration['chrome'],
    ];

    $form['display_options']['width'] = [
      '#type' => 'number',
      '#title' => t('Width'),
      '#description' => t('Leave 0 for auto width.'),
      '#default_value' => $configuration['width'],
      '#field_suffix' => 'px',
    ];

    $form['display_options']['height'] = [
      '#type' => 'number',
      '#title' => t('Height'),
      '#default_value' => $configuration['height'],
      '#field_suffix' => 'px',
    ];

    $form['display_options']['tweet_limit'] = [
      '#type' => 'number',
      '#title' => t('Tweet limit'),
      '#description' => t('The height parameter has no effect when a Tweet limit is set. Leave 0 for no limit.'),
      '#default_value' => $configuration['tweet_limit'],
      '#min' => 0,
      '#max' => 20
    ];

    $form['display_options']['show_replies'] = [
      '#type' => 'checkbox',
      '#title' => t('Show replies'),
      '#default_value' => $configuration['show_replies'],
    ];

    $form['display_options']['aria_polite'] = [
      '#type' => 'radios',
      '#title' => t('Aria polite'),
      '#description' => t('Apply the specified aria-polite behavior.'),
      '#options' => [
        'polite' => t('Polite'),
        'assertive' => t('Assertive'),
        'rude' => t('Rude'),
      ],
      '#default_value' => $configuration['aria_polite'],
    ];

    $form['display_options']['language'] = [
      '#type' => 'select',
      '#title' => t('Language'),
      '#description' => t('What language would you like to display this in?.'),
      '#options' => $this->getAvailableLanguages(),
      '#default_value' => $configuration['language'],
    ];

    return $form;
  }

  public function getAvailableTypes() {
    return [
      'profile' => t('Profile'),
      'list' => t('List'),
      'collection' => t('Collection'),
      'likes' => t('Likes')
    ];
  }

  public function getAvailableDisplayStyles() {
    return [
      'timeline' => t('Timeline (list)'),
      'grid' => t('Grid'),
    ];
  }

  public function setDependentConfiguration(array &$configuration) {
    if (!in_array($configuration['type'], ['list', 'collection'])) {
      $configuration['type_value'] = '';
    }

    if ($configuration['type'] != 'collection') {
      $configuration['display_style'] = 'timeline';
    }
  }

  public function setSettingsFormStates(array $form, $selector) {
    $form['type_value']['#states'] = [
      'visible' => [
        ['input[name="' . $selector . '[type]"]' => ['value' => 'list']],
        ['input[name="' . $selector . '[type]"]' => ['value' => 'collection']]
      ],
      'required' => [
        ['input[name="' . $selector . '[type]"]' => ['value' => 'list']],
        ['input[name="' . $selector . '[type]"]' => ['value' => 'collection']]
      ],
    ];

    $form['display_style']['#states'] = [
      'visible' => [
        ['input[name="' . $selector . '[type]"]' => ['value' => 'collection']]
      ],
    ];
    return $form;
  }
}
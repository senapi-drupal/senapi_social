<?php


namespace Drupal\senapi_social;


class FacebookTimelineWidget extends FacebookWidget implements FacebookWidgetInterface {

  public function getAvailableSettings() {
    return [
      'username' => '',
      'type' => 'profile',
      'type_value' => '',
      'display_style' => 'timeline',
      'display_options' => [
        'href' => '',
        'width' => 340,
        'height' => 500,
        'tabs' => 'timeline',
        'hide_cover' => FALSE,
        'show_facepile' => TRUE,
        'hide_cta' => FALSE,
        'small_header' => FALSE,
        'adapt_container_width' => TRUE,
      ]
    ];
  }

  public static function getDefaultSettings() {
    return [
      'username' => '',
      'type' => 'profile',

      //'href' => '',
      'width' => 340,
      'height' => 500,
      'tabs' => 'timeline',
      'hide_cover' => FALSE,
      'show_facepile' => TRUE,
      'hide_cta' => FALSE,
      'small_header' => FALSE,
      'adapt_container_width' => TRUE,
    ];
  }

  public function getSettingsForm(array $configuration) {
    $form = [];

    $form['type'] = [
      '#type' => 'radios',
      '#title' => t('Type'),
      '#options' => $this->getAvailableTypes(),
      '#default_value' => $configuration['type'],
      '#required' => TRUE,
    ];

    $form['display_options'] = [
      '#type' => 'details',
      '#title' => t('Display options'),
      '#open' => FALSE,
    ];

    $form['display_options']['width'] = [
      '#type' => 'number',
      '#title' => t('Width'),
      '#description' => t('min width 180 a max width 500.'),
      '#default_value' => $configuration['width'],
      '#field_suffix' => 'px',
    ];

    $form['display_options']['height'] = [
      '#type' => 'number',
      '#title' => t('Height'),
      '#description' => t('min width 70.'),
      '#default_value' => $configuration['height'],
      '#field_suffix' => 'px',
    ];

    $form['display_options']['tabs'] = [
      '#type' => 'radios',
      '#title' => t('Tabs'),
      '#options' => [
        'timeline' => t('Timeline'),
        'events' => t('Events'),
      ],
      '#default_value' => $configuration['tabs']
    ];

    $form['display_options']['hide_cover'] = [
      '#type' => 'checkbox',
      '#title' => t('Hide cover'),
      '#default_value' => $configuration['hide_cover'],
    ];

    $form['display_options']['show_facepile'] = [
      '#type' => 'checkbox',
      '#title' => t('Show facepile'),
      '#default_value' => $configuration['show_facepile'],
    ];

    $form['display_options']['hide_cta'] = [
      '#type' => 'checkbox',
      '#title' => t('Hide cta'),
      '#default_value' => $configuration['hide_cta'],
    ];

    $form['display_options']['small_header'] = [
      '#type' => 'checkbox',
      '#title' => t('Small header'),
      '#default_value' => $configuration['small_header'],
    ];

    $form['display_options']['adapt_container_width'] = [
      '#type' => 'checkbox',
      '#title' => t('Adapt container width'),
      '#default_value' => $configuration['adapt_container_width'],
    ];

    return $form;
  }

  public function getAvailableTypes() {
    return [
      'profile' => t('Profile'),
    ];
  }

  public function getAvailableDisplayStyles() {
    return [];
  }

  public function setDependentConfiguration(array &$configuration) {
    // TODO: Implement setDependentConfiguration() method.
  }

  public function setSettingsFormStates(array $form, $selector) {
    return $form;
  }
}
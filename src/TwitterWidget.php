<?php


namespace Drupal\senapi_social;

use Drupal\Core\Url;

/**
 * Class TwitterWidget
 */
abstract class TwitterWidget implements TwitterWidgetInterface {

  /**
   * Get all available languages.
   *
   * @return array
   *   Array of languages.
   */
  public function getAvailableLanguages() {
    return [
      'en' => t('English (default)'),
      'es' => t('Spanish'),
    ];
  }

  /**
   * Returns a Twitter widget depending on the configuration.
   *
   * @param array $configuration
   *   List of selected configuration.
   *
   * @return array
   *   The Twitter widget as a render array.
   */
  public function getWidget(array $configuration) {
    $build['twitter_widget'] = [
      '#type' => 'link',
      '#title' => $this->createLabel($configuration),
      '#url' => $this->createUrl($configuration),
      '#attributes' => $this->createAttributes($configuration),
      '#attached' => [
        'library' => ['senapi_social/twitter_widgets']
      ]
    ];

    return $build;
  }

  /**
   * @param array $configuration
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  private function createLabel(array $configuration) {
    return t('Tweets por @@username', ['@username' => $configuration['username']]);
  }

  /**
   * @param array $configuration
   *
   * @return \Drupal\Core\Url
   */
  private function createUrl(array $configuration) {
    $uri = 'https://twitter.com/' . $configuration['username'];

    switch ($configuration['type']) {
      case 'list':
        $uri .= '/lists/' . $configuration['type_value'];
        break;

      case 'collection':
        $uri .= '/timelines/' . $configuration['type_value'];
        break;

      case 'likes':
        $uri .= '/likes';
        break;
    }

    return Url::fromUri($uri);
  }


  private function createAttributes(array $configuration) {
    $result = [];
    $result['class'] = ['twitter-' . $configuration['display_style']];
    if (!empty($configuration['language'])) {
      $result['lang'] = $configuration['language'];
    }

    if (!empty($configuration['theme'])) {
      $result['data-theme'] = $configuration['theme'];
    }

    if (!empty($configuration['chrome'])) {
      $options = array_keys(array_filter($configuration['chrome']));
      if (count($options)) {
        $result['data-chrome'] = implode(' ', $options);
      }
    }

    if (!empty($configuration['width'])) {
      $result['data-width'] = $configuration['width'];
    }

    if (!empty($configuration['height'])) {
      $result['data-height'] = $configuration['height'];
    }

    if (!empty($configuration['link_color'])) {
      $result['data-link-color'] = $configuration['link_color'];
    }

    if (!empty($configuration['border_color'])) {
      $result['data-border-color'] = $configuration['border_color'];
    }

    if (!empty($configuration['tweet_limit'])) {
      $result['data-tweet-limit'] = $configuration['tweet_limit'];
    }

    if (!empty($configuration['aria_polite'])) {
      $result['aria-polite'] = $configuration['aria_polite'];
    }

    if ($configuration['hide_username']) {
      $result['data-show-screen-name'] = 'false';
    }

    if ($configuration['hide_followers_count']) {
      $result['data-show-count'] = 'false';
    }

    if (!empty($configuration['size'])) {
      $result['data-size'] = 'large';
    }

    return $result;
  }
}
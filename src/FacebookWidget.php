<?php


namespace Drupal\senapi_social;


use Drupal\Core\Url;

abstract class FacebookWidget implements FacebookWidgetInterface {

  public function getAvailableLanguages() {
    return [
      'en' => t('English (default)'),
      'es' => t('Spanish'),
    ];
  }

  public function getWidget(array $configuration) {
    $build['facebook'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => ['fb-root']
      ],
    ];

    $build['facebook_widget'] = [
      '#type' => 'container',
      '#attributes' => $this->createAttributes($configuration),
    ];

    $build['facebook_widget']['link'] = [
      '#type' => 'link',
      '#title' => $this->createLabel($configuration),
      '#url' => $this->createUrl($configuration),
      '#prefix' => '<blockquote cite="' . $this->createUrl($configuration)->getUri() . '" class="fb-xfbml-parse-ignore">',
      '#suffix' => '</blockquote>',
      '#attached' => [
        'library' => ['senapi_social/facebook_widgets'],
        'drupalSettings' => [
          //'facebookId' => $configuration['app_id']
        ]
      ]
    ];

    return $build;
  }

  private function createLabel(array $configuration) {
    return t('Facebook @username', ['@username' => $configuration['username']]);
  }

  private function createUrl(array $configuration) {
    $uri = 'https://www.facebook.com/' . $configuration['username'];
    return Url::fromUri($uri);
  }
  private function createAttributes(array $configuration){
    $result = [];
    $result['class'] = ['fb-page'];

    if (!empty($configuration['username'])) {
      $result['data-href'] = $this->createUrl($configuration)->getUri();
    }

    if (!empty($configuration['tabs'])) {
      $result['data-tabs'] = $configuration['tabs'];
    }

    if (!empty($configuration['width'])) {
      $result['data-width'] = $configuration['width'];
    }

    if (!empty($configuration['height'])) {
      $result['data-height'] = $configuration['height'];
    }

    if (!empty($configuration['small_header'])) {
      $result['data-small-header'] = $configuration['small_header'];
    }

    if (!empty($configuration['adapt_container_width'])) {
      $result['data-adapt-container-width'] = $configuration['adapt_container_width'];
    }

    if (!empty($configuration['hide_cover'])) {
      $result['data-hide-cover'] = $configuration['hide_cover'];
    }

    if (!empty($configuration['show_facepile'])) {
      $result['data-show-facepile'] = $configuration['show_facepile'];
    }

    return $result;
  }
}
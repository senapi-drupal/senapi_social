(function ($, Drupal, window, document, undefined) {
    Drupal.behaviors.senapi_social_telegram = {
        attach: function (context, settings) {
            if (context !== document) {
                // AJAX request.
                return;
            }

            $(document).ready(function () {
                setTimeout(function () {
                    // telegram
                    if (!settings.replainId) {
                        return;
                    }

                    window.replainSettings = { id: settings.replainId };
                    (function(u){
                        var s=document.createElement('script');
                        s.type='text/javascript';
                        s.async=true;
                        s.src=u;
                        var x=document.getElementsByTagName('script')[0];
                        x.parentNode.insertBefore(s,x);
                    })('https://widget.replain.cc/dist/client.js');

                },1500);
            });
        }
    };
})(jQuery, Drupal, this, this.document);
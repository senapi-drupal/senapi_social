(function ($, Drupal, window, document, undefined) {
    Drupal.behaviors.senapi_social_twitter = {
        attach: function (context, settings) {
            if (context !== document) {
                // AJAX request.
                return;
            }

            $(document).ready(function () {
                setTimeout(function () {
                    // twitter
                    (function(d, s, id) {

                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) {
                            return;
                        }
                        js = d.createElement(s);
                        js.id = id;
                        js.src = '//platform.twitter.com/widgets.js';
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'twitter-jssdk', settings));
                },1500);
            });
        }
    };
})(jQuery, Drupal, this, this.document);